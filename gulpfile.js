var gulp = require('gulp');
var concat = require('gulp-concat');
var frontBower = './frontend/bower_components/';

gulp.task('default', function() {
  
});

gulp.task('front', function() {
  return gulp.src([
    frontBower + 'jquery/dist/jquery.js',
    frontBower + 'Materialize/dist/js/materialize.js',
    frontBower + 'vue/dist/vue.js',
    frontBower + 'vue-router/dist/vue-router.js',
    './frontend/js/*.js',])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./frontend/lib/'));  
});

gulp.task('back', function() {
});

gulp.task('watch_front', function() {
  gulp.watch('frontend/*')
});

gulp.task('watch_back', function() {
  gulp.watch('backend/*', ['back']);
});